<?php

use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('courses')->truncate();

        $users = [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ];

        /**
         * Create an array containing all course names
         */
        $courses = [
            'Computing',
            'Software & Systems',
            'Web',
            'Application Development'
        ];

        /**
         * Loop though courses and create a course with random data for each course
         */
        foreach($courses as $key => $courseName) {

            /**
             * Create new randomized array to shuffle users
             */
            $userNumber = array_rand($users);

            /**
             * Add database entry for new course assigning course name, random course code and random course leader
             */
            DB::table('courses')->insert([
                'title' => $courseName,
                'code' => 'BIS' . rand(1000000, 99999999),
                'leader' => $users[$userNumber]
            ]);

        }

    }
}
