@extends('layouts.master')
<!-- resources/views/auth/login.blade.php -->
@section('title', 'CourseAdmin')
@stop

@section('content')
    <h1>CourseAdmin</h1>
    <div class="large-12 columns">
        <form method="POST" action="{{ url('admin/login') }}">
            {!! csrf_field() !!}

            <div class="large-12 columns">
                Email
                <input type="email" name="email" value="{{ old('email') }}">
            </div>

            <div class="large-12 columns">
                Password
                <input type="password" name="password" id="password">
            </div>

            <div class="large-12 columns">
                <input type="checkbox" name="remember"> Remember Me
            </div>

            <div class="large-12 columns">
                <button type="submit">Login</button>
            </div>
        </form>
    </div>
@stop