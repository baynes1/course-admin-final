@extends('layouts.master')
@section('title')
    Module - {{ $module->code }} - {{ $module->title }}
@stop
@section('content')

    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
        </div>
    @endif

    <h1 class="small-12 columns">{{ $module->title }} <small>({{ Progress::percentage($completedItems->count(), $module->items()->count(), TRUE)}})</small></h1>

    <!-- Add the progress bar  -->
    <div class="progress small-12 columns">
        <span class="meter" style="width:{{ Progress::percentage($completedItems->count(), $module->items()->count(), TRUE)}};"></span>
    </div> 




    <div class="small-12 columns">
        <p> <strong>Module code:</strong> {{ $module->code }}</p>
        <p> <strong>Module Leader:</strong> {{ $moduleleader->name }}</p>
        <div>
            <h2 class="row">Associated Items</h2>
            @if ( !$module->items->count() )
                Your Module has no Items linked.
            @else
                <ul class="no-bullet small-12 columns">
                    @foreach( $module->items as $item )
                        <li class="row">
                            <p class="small-8 columns">{{ $item->text }}</p>
                                <!-- <form method="PUT" action="{{url('/admin/itemmodule') }}" enctype="multipart/form-data" id="form" data-abide> -->
                            <div class="small-4 columns button-group">
                            {!! Form::model($item, array('method' => 'put', 'route' => ['admin.itemmodule.update', $item->id], 'data-abide' => '')) !!}
                                <input type="hidden" name="item_id" value="{{ $item->id }}" />
                                <input type="hidden" name="module_id" value="{{ $module->id }}" />
                                <!-- switch the buttons based on state of item -->
                                @if ($item->pivot->complete != '1')
                                <!-- completed submit button -->
                                <button type="submit" name="{{$item->id}}markcomplete" class="button right tiny">Complete</button>
                                <input type="hidden" name="complete" value="1" />
                                @else
                                <!-- uncompleted submit button -->
                                <input type="submit" name="{{$item->id}}markuncomplete" class="button right tiny" value="Incomplete" />
                                <input type="hidden" name="complete" value="0" />
                                @endif
                            {!! Form::close() !!}

                            {!! Form::open([ 'route' => ['admin.itemmodule.destroy', $item->id], 'method' => 'DELETE'  ]) !!}
                                <input type="hidden" name="module_id" value="{{ $module->id }}">
                                <input type ="submit" value="Remove" class="button right tiny alert"> 
                            {!! Form::close() !!}
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
        
    </div>
    <div class="small-12 columns">
        <div class="row clearfix">
            <h3>Add this item to this module</h3>
            {!! Form::open([ 'route' => ['admin.itemmodule.store', $module->id] ]) !!}

            <div class="columns small-12">
                <select name="item_id">
                    <option value="">Select an item...</option>
                    @foreach ($items as $item)
                    <option value="{{ $item->id }}">{{ $item->text }}</option>
                    @endforeach
                </select>
            </div>

            <div class="columns small-12">
                {!! Form::hidden('module_id', $module->id) !!}
                <button type="submit" class="button">Add Item</button>
            </div>

            {!! Form::close() !!}


        </div>

        <a href="{{ route('admin.modules.edit', $module->id) }}" class="button small warning right">Edit Module</a>
    </div>
@stop