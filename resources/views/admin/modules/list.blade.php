@extends('layouts.master')
@section('title')
    All Modules
@stop
@section('content')
    <?php $adminstatus = (Auth::user()->admin) ?>
    @if ($adminstatus == '1')
        <h1>Modules <a href="{{ url('admin/modules/create') }}" class="button right success small">
            Add a new module
        </a></h1>
    @else
        <h1>Modules</h1>
    @endif

    @if(Auth::check())
            <!--  The user is logged in... -->
    <div class="row small-12 columns">

        @if ($adminstatus == '1' || $adminstatus == '2')
                <!-- return a list of all courses -->
        <div class="small-12 columns">
            @foreach($allModules as $aModule)
                <div class="row">
                    {!! Form::open(array('url' => '/admin/modules/' . $aModule->id)) !!}
                    <a href="{{ route('admin.modules.show', [$aModule->id]) }}" class="item">{{ $aModule->title }}</a>

                    <div class="progress">
                                <span class="meter"  style="width:{{ Progress::percentage($completedItems->where('module_id', $aModule->id)->count(), $aModule->items()->count(), TRUE) }}"></span>
                    </div>


                    {!! Form::hidden('_method', 'DELETE') !!}
                    {!! Form::submit('Delete this Module', array('class' => 'button tiny alert right', 'id' => $aModule->title . ' delete', 'name' => $aModule->title . ' delete')) !!}
                    {!! Form::close() !!}
                    <hr />
                </div>
            @endforeach

        </div>
        @else

                <!-- return only courses the staff are course leaders of and if none then return a message saying none to show. -->
        <?php $myModules = $allModules->where('leader', (Auth::user()->id)) ?>
        <ul class="no-bullet">
            @foreach ($myModules as $myModule)
                <li><a href="{{ route('admin.modules.show', [$myModule->id]) }}" class="item">{{ $myModule->title }}</a></li>
            @endforeach
        </ul>

        @endif
    </div>

    @endif



@stop