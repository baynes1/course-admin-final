@extends('layouts.master')
@section('title')
    All Courses
@stop
@section('content')
    <?php $adminstatus = (Auth::user()->admin) ?>


    <h1 class="small-12 columns">Courses @if ($adminstatus == '1') <a href="{{ url('admin/courses/create') }}" class="button small success right">
            Add a new course
        </a> @endif</h1>

    @if(Auth::check())
        <!--  The user is logged in... -->
    <div class="row small-12 columns">

        @if ($adminstatus == '1' || $adminstatus == '2')

            <!-- return a list of all courses -->
            <div class="small-12 columns">
                @foreach($allCourses as $aCourse)
                    <div class="row">
                        {!! Form::open(array('url' => '/admin/courses/' . $aCourse->id)) !!}
                        <a href="{{ route('admin.courses.show', [$aCourse->id]) }}" class="item">{{ $aCourse->title }}</a>
                        {!! Form::hidden('_method', 'DELETE') !!}
                        {!! Form::submit('Delete this Course', array('class' => 'button tiny alert right', 'id' => $aCourse->title . ' delete', 'name' => $aCourse->title . ' delete')) !!}
                        {!! Form::close() !!}
                        <hr />

                    </div>

                @endforeach
                </div>

        @else

        <!-- return only courses the staff are course leaders of and if none then return a message saying none to show. -->
            <?php $myCourses = $allCourses->where('leader', (Auth::user()->id)) ?>
            <div class="small-12 columns">
                @foreach ($myCourses as $myCourse)
                    <div class="row">
                        <a href="{{ route('admin.courses.show', [$myCourse->id]) }}" class="item">{{ $myCourse->title }}</a>
                    </div>
                @endforeach
            </div>

        @endif
    </div>

    @endif



@stop