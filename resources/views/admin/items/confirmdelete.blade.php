@extends('layouts.master')
@section('title', 'Delete ' . $item->item_id)

@section('content')

	{!! Form::open(['route' => ['admin.itemmodule.destroy', $item->item_id], 'method' => 'DELETE' ]) !!}


	<div class="warning alert-box large-12">
		<p>Are you sure you want to remove this item from the module?</p>
		<div class="button-group">
			<input type="submit" value="Sure, Remove it." class="button"> 
			<input type="hidden" value="{{ $item->module_id}}" name="module_id">
			<input type="hidden" value="1" name="confirm_stage">
 			<a href="{{ url('admin/modules/' . $item->module_id) }}" class="button alert">NO! Go Back.</a>
		</div>
	</div>
	{!! Form::close() !!}


@stop