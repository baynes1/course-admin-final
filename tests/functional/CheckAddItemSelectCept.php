<?php 
$I = new FunctionalTester($scenario);

$I->am('a module leader')
$I->wantTo('see the check add item select on the page');

Auth::loginUsingId(12);
$I->seeAuthentication();

$I->amOnPage('/admin/modules/2');

$I->see('Add this item to this module', 'h3');
$I->seeElement('select[name="item_id"]');