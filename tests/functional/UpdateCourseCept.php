<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('update a course');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create a new course...
$I->haveRecord('courses', [
    'id' => 5,
    'title' => 'Web Design and Development',
    'code' => 'BIS11111111',
    'leader' => 1,
]);
// When
$I->amOnPage('/admin/courses/5');
// Then
$I->click('Edit Course');
//Then
$I->amOnPage('/admin/courses/5/edit');
$I->see('Edit - Web Design and Development', 'h1');
// And
$I->amGoingTo('Clear the code field in order to submit an invalid form');
// When
$I->fillField('code', null);
$I->click('Update Course');

// the above could and probably should be expanded to run a separate test on each form filed.

// Then
$I->expectTo('See the form again with the errors');
$I->seeCurrentUrlEquals('/admin/courses/5/edit');
$I->see('The code field is required');
// Then
$I->fillField('code', 'BIS11111111');
$I->selectOption('courseleader', 12);
//  And
$I->click('Update Course');

// Then
$I->seeCurrentUrlEquals('/admin/courses/5');
$I->see('Web Design and Development', 'h1');
$I->seeRecord('courses', [
    'id' => 5,
    'title' => 'Web Design and Development',
    'code' => 'BIS11111111',
    'leader' => 12,
]);