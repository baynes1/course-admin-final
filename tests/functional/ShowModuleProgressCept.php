<?php 
$I = new FunctionalTester($scenario);

$I->am('a Module Leader');
$I->wantTo('see all module progesses');

Auth::loginUsingId(11);
$I->seeAuthentication();

$I->amOnpage('/admin/modules');
$I->see('0%', '.progress');