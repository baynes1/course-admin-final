<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('delete a course');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// When
$I->amOnPage('/admin/courses');
// And
$I->see('Web', 'a.item');
$I->seeRecord('courses', [
    'title' => 'Web']
);
// Then
$I->click('Web delete');
// Then
$I->seeCurrentUrlEquals('/admin/courses');
$I->dontSee('Web', 'a.item');