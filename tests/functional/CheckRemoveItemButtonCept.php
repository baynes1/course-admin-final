<?php 
$I = new FunctionalTester($scenario);

$I->am('a module leader')
$I->wantTo('see the remove item button on the page');

//Login
Auth::loginUsingId(12);
$I->seeAuthentication();

$I->amOnPage('/admin/modules/2');

$I->see('Remove', 'button');
$I->see('Complete', 'button');
