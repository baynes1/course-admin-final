<?php 

namespace App\Helpers;

class ProgressHelper {

	public static function percentage($count, $total, $sign = FALSE)
	{	
		//If the count is less than 1, show 0
		//If the sign option is true, show
		//a percentage sign.
		if ($count < 1) return 0 . (($sign) ? '%' : NULL);

		//Otherwise, return the percentage
		//and if the sign is set to true
		//the percentage symbol too.
		return round(($count/$total) * 100) . (($sign) ? '%' : NULL);
	}


}