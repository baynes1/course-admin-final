<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    /**
     * Handle the forbidden response by sending a 403 error.
     *
     * @see \Illuminate\Foundation\Http\FormRequest::forbiddenResponse()
     *
     * @return Response 403 error
     */
    public function forbiddenResponse()
    {
        return abort(403);
    }
}
