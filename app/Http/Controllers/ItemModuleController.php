<?php

namespace App\Http\Controllers;

use App\Item;
use App\Module;
use App\ItemModule;
use Illuminate\Http\Request;
use Redirect;
use Carbon;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemModuleController extends Controller
{

    public function index()
    {
        //
    }

    public function create($id)
    {
        return 'Item' . $id;
    }

    public function store(Request $request)
    {
        $data = [
            'item_id'   => $request->input('item_id'),
            'module_id' => $request->input('module_id'),
            'complete' => 0
        ];
        $itemmodule = new ItemModule($data);
        $itemmodule->save();
        return redirect('admin/modules/' . $request->input('module_id'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {

        /*$module_id = $request->get('module_id');

        $itemmodule = ItemModule::findORFail($id);

        $itemmodule->update([
            'completed' => $request->get('completed')
        ]);*/

        // dd($request->all());

        $GLOBALS['module_id'] = $request->get('module_id');
        $GLOBALS['item_id'] = $request->get('item_id');
        $GLOBALS['completed'] = $request->input('complete');
        $module_id = $request->get('module_id');
        $mytime = Carbon\Carbon::now();

        $itemModule = DB::table('item_module')->where('module_id', $GLOBALS['module_id'])->where('item_id', $GLOBALS['item_id'])->update(['complete' => $GLOBALS['completed'], 'updated_at' => $mytime]);


        //dd($itemModule);

        $status = (($GLOBALS['completed'] == 1) ? 'complete' : 'incomplete');


        return redirect()->to('/admin/modules/'.$module_id)->with('message', 'Your item has been marked as ' . $status.'!');
    }

    public function destroy($id, Request $request)
    {
        $item = Itemmodule::where('item_id', $id)
                                ->where('module_id', $request->input('module_id'))
                                ->first();
        //Keep the module ID so it is accessible after the item has been deleted.
        $module_id = $item->module_id;

        if ($request->has('confirm_stage'))
        {
            //code to delete
            DB::table('item_module')
            ->where('item_id', $item->item_id)
            ->where('module_id', $item->module_id)
            ->delete();
            
            return redirect('admin/modules/' . $module_id);
        }
        else
        {
            //code to confirm
            

            return view('admin.items.confirmdelete', compact('item'));
        }
    }

}
