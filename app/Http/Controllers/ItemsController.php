<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // return a list of all items in descending order
        $allItems = Item::all()->sortByDesc('id');
        return view('admin.items.list', ['allItems' => $allItems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\CreateItemsRequest $request)
    {
        $item = new Item;

        $item->text = $request->text;
        $item->default   = $request->default;

        $item->save();
        return Redirect::route('admin.items.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $item = Item::findOrFail($id);

        return view ('admin.items.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);

        // pass over two arrays/variables to the view.
        return view('admin.items.edit', ['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\CreateItemsRequest $request, $id)
    {
        $item = Item::findORFail($id);

        $item->update([
            'text' => $request->get('text'),
            'default' => $request->get('default')
        ]);

        return Redirect::route('admin.items.index')->with('message', 'Your item has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Item::destroy($id);

        // redirect
        return \Redirect::route('admin.items.index')->with('message', 'Your Item was deleted!');
    }
}
