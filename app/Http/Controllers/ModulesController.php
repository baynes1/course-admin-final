<?php

namespace App\Http\Controllers;
use App\User;
use App\ItemModule;
use App\Module;
use App\Item;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;


class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // return a list of all modules
        $allModules = Module::all();

        $completedItems = itemModule::where('complete', 1)->get();


        return view('admin.modules.list', compact('allModules', 'completedItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // data and view for creating a module
        $users = User::where('admin', 0)
            ->orderBy('name', 'desc')
            ->get();

        $itemModuleAssociations = Module::itemModuleAssociations(0);

        return view('admin.modules.create', ['users' => $users])->with(['itemModuleAssociations' => $itemModuleAssociations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\CreateModulesRequest $request)
    {
        $module = new Module;

        $module->title = $request->title;
        $module->code = $request->code;
        $module->leader = $request->moduleleader;

        $module->save();

        $itemModules = $request->get('itemModules');

        // write module associations
        if (count($itemModules) > 0 ) {
            foreach ($itemModules as $item_id) {
                DB::table('item_module')->insert([
                    'module_id' => $module->id, 'item_id' => $item_id
                ]);
            }
        }

        return Redirect::route('admin.modules.show', [$module]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // return a single module based on module id
        $module = Module::findORFail($id);
        $moduleleader = User::find($module->leader);
        $items = Item::get();

        $completedItems = itemModule::where('module_id', $id)
                                        ->where('complete', 1)
                                        ->get();


        return view('admin.modules.show', compact('module', 'moduleleader', 'completedItems', 'items'));

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $users = User::where('admin', 0)
            ->orderBy('name', 'desc')
            ->get();
        $userselector = array();
        foreach($users as $user) {
            $userselector[$user->id] = $user->name;
        };
        $module = Module::findOrFail($id);
        // pass over two arrays/variables to the view.

        $itemModuleAssociations = Module::itemModuleAssociations($id);

        return view('admin.modules.edit', ['module' => $module])->with(['userselector' => $userselector])->with(['itemModuleAssociations' => $itemModuleAssociations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\CreateModulesRequest $request, $id)
    {
        $module = Module::findORFail($id);

        $module->update([

            'title' => $request->get('title'),
            'code' => $request->get('code'),
            'leader' => $request->get('moduleleader')
        ]);


        $itemModuleAssociations = Module::itemModuleAssociations($id);
        $itemModules = $request->get('itemModules');

        foreach ($itemModuleAssociations as $item)
        {
            $skipped = false;
            // error capture for having no items associated
            if (count($itemModules) > 0 ) {
                // check if association already exists if it does remove it from the $courseModules array then remove it frm the db
                if ($item->checked == 1 && (($key = array_search($item->id, $itemModules)) !== false)) {
                    unset($itemModules[$key]);
                    $skipped = true;
                }
            }
            // delete any removed associations
            if($item->checked == 1 && !$skipped)
            {
                DB::table('item_module')->where('module_id', '=', $id)->where('item_id', '=', $item->id)->delete();
            }
            unset($key);
        }
        // write module associations
        if (count($itemModules) > 0 ) {
            foreach ($itemModules as $item_id) {
                DB::table('item_module')->insert([
                    'module_id' => $id, 'item_id' => $item_id
                ]);
            }
        }

        return Redirect::route('admin.modules.show', $module->id)->with('message', 'Your module has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        Module::destroy($id);

        // redirect
        //Session::flash('message', 'Successfully deleted the Module!');
        return \Redirect::route('admin.modules.index');
    }


}
